import socket

HEADERSIZE = 10

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostbyaddr('192.168.43.63')[0], 1242))
s.listen(20)

while True:
    # now our endpoint knows about the OTHER endpoint.
    clientsocket, address = s.accept()
    print(f"Connection from {address} has been established.")
    #print(f"Socket {clientsocket} has been established.")
    msg = "Welcome to the server!"
    msg = f"{len(msg):<{HEADERSIZE}}"+msg
    clientsocket.send(bytes(msg,"utf-8"))
    ch=input("Want to send another message(Y/N):")
    if ch == 'Y':
    #    msg1=input("Type new message:")
    #    msg1 = f"{len(msg1):<{HEADERSIZE}}"+msg1
    #    print("Constructed msg:"+msg1)
    #    clientsocket.send(bytes(msg1,"utf-8"))
