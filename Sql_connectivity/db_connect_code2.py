#Before u can import mysql.connector istall mysql-connector-python package
import mysql.connector

#Create the connection object
myconn = mysql.connector.connect(host = "localhost", user = "anupkunte",passwd = "asdf@123",database="user_login_check")

#creating the cursor object
cur = myconn.cursor()
print(cur)
try:
    dbs = cur.execute("show databases")
    print("First Time"+str(cur))
except:
    myconn.rollback()

for x in cur:
    print(x[0])
#print(dbs)


try:
    dbs = cur.execute("show tables")
    print("Second Time"+str(cur))
except e:
    print(e)
    myconn.rollback()

for x in cur:
    print(x[0])

myconn.close()

''' Sample output

~$python db_connect_code2.py
CMySQLCursor: (Nothing executed yet)
First TimeCMySQLCursor: show databases
information_schema
user_login_check
Second TimeCMySQLCursor: show tables
user

'''
