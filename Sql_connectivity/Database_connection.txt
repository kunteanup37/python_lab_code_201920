~$mysql -u anupkunte -p user_login_check
Enter password: 

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| user_login_check   |
+--------------------+
2 rows in set (0.00 sec)

mysql> show tables;
+----------------------------+
| Tables_in_user_login_check |
+----------------------------+
| user                       |
+----------------------------+
1 row in set (0.01 sec)

mysql> describe user;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| login_id | varchar(16) | YES  |     | NULL    |       |
| passwd   | varchar(16) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

mysql> select * from user;
+----------+---------+
| login_id | passwd  |
+----------+---------+
| anup     | Ask@123 |
| adutya   | Adk@123 |
| ajinkya  | Ak@123  |
+----------+---------+
3 rows in set (0.00 sec)

