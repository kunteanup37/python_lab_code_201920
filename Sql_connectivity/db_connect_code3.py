#Before u can import mysql.connector istall mysql-connector-python package
import mysql.connector

#Create the connection object
myconn = mysql.connector.connect(host = "localhost", user = "anupkunte",passwd = "asdf@123",database="user_login_check")

#creating the cursor object
cur = myconn.cursor()

# Get User data to be inserted into login information database
while True:
    username=input("\n Enter Userid:")
    passwd=input("\n Enter User password:")

    #build SQL query
    sql = "insert into user(login_id, passwd) values (%s, %s)"
    #The row values are provided in the form of tuple
    val = (username,passwd)
    try:
        print("Inserting records and performing commit")
        dbs = cur.execute(sql,val)
        myconn.commit()
    except:
        print("Exception occured Rolling back transaction.....")
        myconn.rollback()
        break;
    print("Recored Inserted Successfully...")
    ch=input("Want to insert more records(Y|y):")
    #print(ch)
    if ch == 'Y' or ch == 'y':
        ch=''
        continue;
    else:
        break;
print("Closing Connection to Database....")
myconn.close()
print("Connection to Database Ended Successfully....")
''' Sample output
Intial content of DB

mysql> select * from user;
+----------+----------+
| login_id | passwd   |
+----------+----------+
| anup     | anup@123 |
| animesh  | anish123 |
| user1    | user@123 |
| user2    | user@123 |
| user3    | user@123 |
| user4    | user@123 |
| user5    | user@123 |
+----------+----------+
7 rows in set (0.00 sec)

~$python db_connect_code3.py

 Enter Userid:user6

 Enter User password:user@123
Inserting records and performing commit
Recored Inserted Successfully...
Want to insert more records(Y|y):y

 Enter Userid:user7

 Enter User password:user@123
Inserting records and performing commit
Recored Inserted Successfully...
Want to insert more records(Y|y):n
Closing Connection to Database....
Connection to Database Ended Successfully....

mysql> select * from user;
+----------+----------+
| login_id | passwd   |
+----------+----------+
| anup     | anup@123 |
| animesh  | anish123 |
| user1    | user@123 |
| user2    | user@123 |
| user3    | user@123 |
| user4    | user@123 |
| user5    | user@123 |
| user6    | user@123 |
| user7    | user@123 |
+----------+----------+
9 rows in set (0.00 sec)


'''
