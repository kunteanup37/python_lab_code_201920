#Before u can import mysql.connector istall mysql-connector-python package
import mysql.connector

#Create the connection object
myconn = mysql.connector.connect(host = "localhost", user = "anupkunte",passwd = "asdf@123",database="user_login_check")

#creating the cursor object
cur = myconn.cursor()

#list of tuples of val type to be inserted in database
values=[]

# sql query to insert data int database
sql = "insert into user(login_id, passwd) values (%s, %s)"

# Get User data to be inserted into login information database
ch=''
while True:
    username=input("\n Enter Userid:")
    passwd=input("\n Enter Userpassword:")
    val = (username,passwd)
    values.append(val)
    ch=input("Want to insert more records(type n to quit):")
    if ch =='N' or ch=='n':
        break;
    #print(ch)
try:
    print("Inserting records and performing commit")
    dbs = cur.executemany(sql,values)
    #Alternate way for line 29 above
    #for val in values:
    #    dbs = cur.execute(sql,val)
    myconn.commit()
    print(cur.rowcount," records inserted simultaneously!")
except e:
    print("Exception occured Rolling back transaction.....")
    print(e)
    myconn.rollback()
print("Recored Inserted Successfully...")
print("Closing Connection to Database....")
myconn.close()
print("Connection to Database Ended Successfully....")
''' Sample output
~$python db_connect_code4.py

 Enter Userid:user1

 Enter Userpassword:user@123
Want to insert more records(type n to quit):y
y

 Enter Userid:user2

 Enter Userpassword:user@123
Want to insert more records(type n to quit):y
y

 Enter Userid:user3

 Enter Userpassword:user@123
Want to insert more records(type n to quit):n
Inserting records and performing commit
3  records inserted simultaneously!
Recored Inserted Successfully...
Closing Connection to Database....
Connection to Database Ended Successfully....

Database table content before :

mysql> select * from user;
+----------+----------+
| login_id | passwd   |
+----------+----------+
| anup     | anup@123 |
| animesh  | anish123 |
+----------+----------+

Database table content after :

mysql> select * from user;
+----------+----------+
| login_id | passwd   |
+----------+----------+
| anup     | anup@123 |
| animesh  | anish123 |
| user1    | user@123 |
| user2    | user@123 |
| user3    | user@123 |
+----------+----------+

'''
