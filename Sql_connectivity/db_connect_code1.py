#Before u can import mysql.connector istall mysql-connector-python package
import mysql.connector

#Create the connection object
myconn = mysql.connector.connect(host = "localhost", user = "anupkunte",passwd = "asdf@123",database="user_login_check")

#printing the connection object
print(myconn)

#creating the cursor object
cur = myconn.cursor()
print(cur)

myconn.close()

''' Sample Output
~$python db_connect_code1.py
<mysql.connector.connection_cext.CMySQLConnection object at 0x7f0bd08ff748>
CMySQLCursor: (Nothing executed yet)
'''
