#This script illustrates use of bonary operators in python
a=input("\nEnter first number:")
b=input("\nEnter second number:")
ch=input("\nMenu\n1. Add.\n2.Subtract\n3. Div\n4. Multiply.\nEnter Operation you want to perform:")
if ch == '1':
	res=int(a)+int(b)
elif ch == '2':
	res=int(a)-int(b)
elif ch == '3':
	res=int(a)/int(b)
elif ch == '4':
	res=int(a)*int(b)
else:
	pass
#note how we perform formatted printing using print statement
print("Operand 1: %s Operand 2: %s Operation: %c Result: %f"%(a,b,ch,res))
''' This is multiline comment 
Run1:

$python m1_exp2_arithmatic_op.py 

Enter first number:23

Enter second number:43

Menu
1. Add.
2.Subtract
3. Div
4. Multiply.
Enter Operation you want to perform:1
Operand 1: 23 Operand 2: 43 Operation: 1 Result: 66.000000

Run2:
$python m1_exp2_arithmatic_op.py 

Enter first number:23

Enter second number:43

Menu
1. Add.
2.Subtract
3. Div
4. Multiply.
Enter Operation you want to perform:2
Operand 1: 23 Operand 2: 43 Operation: 2 Result: -20.000000

Run3
$python m1_exp2_arithmatic_op.py 

Enter first number:23

Enter second number:43

Menu
1. Add.
2.Subtract
3. Div
4. Multiply.
Enter Operation you want to perform:3
Operand 1: 23 Operand 2: 43 Operation: 3 Result: 0.534884

Run4
$python m1_exp2_arithmatic_op.py 

Enter first number:23

Enter second number:43

Menu
1. Add.
2.Subtract
3. Div
4. Multiply.
Enter Operation you want to perform:4
Operand 1: 23 Operand 2: 43 Operation: 4 Result: 989.000000
''' 
