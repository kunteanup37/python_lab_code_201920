#define to strings
str1="This is sample program.\nIt illustrates us how does string splitting works.\nIt also shows how we can use dictionaries.\n"
str2="This programs defines two multiline strings namelt str1 and str2.\nWe first join them using + operator."
#concatinate two strings
str=str1+str2
print("Original string:\n"+str)
#split string based on delimiter \n store in list called lines
lines=str.split('\n')
#check what is type of object lines
print(type(lines))
#Define dictionary that counts words per line
wcdict={}
#Define dictionary that counts characters per line
ccdict={}
#counter to know line number it will be key for both dictionaries
lcnt=0
for line in lines:
	words=line.split()
	wcdict[lcnt]=len(words)
	ccdict[lcnt]=len(line)
	lcnt+=1
print(wcdict)
print(ccdict)

#Define a single dict to know words as well as characters count perline
wc_c_dict={}
lcnt=0
for line in lines:
	words=line.split()
	wc_c_dict[lcnt]=[len(words),len(line)]
	lcnt+=1
print(wc_c_dict)
#access key and value by iterating over dictionary
for key, value in wc_c_dict.items():
	print("%d line has %d words and %d numbers of characters"%(key,value[0],value[1]))

''' Output of the script

Original string:
This is sample program.
It illustrates us how does string splitting works.
It also shows how we can use dictionaries.
This programs defines two multiline strings namelt str1 and str2.
 We first join them using + operator.
<class 'list'>
{0: 4, 1: 8, 2: 8, 3: 10, 4: 7}
{0: 23, 1: 50, 2: 42, 3: 65, 4: 37}
{0: [4, 23], 1: [8, 50], 2: [8, 42], 3: [10, 65], 4: [7, 37]}
0 line has 4 words and 23 numbers of characters
1 line has 8 words and 50 numbers of characters
2 line has 8 words and 42 numbers of characters
3 line has 10 words and 65 numbers of characters
4 line has 7 words and 37 numbers of characters
'''

