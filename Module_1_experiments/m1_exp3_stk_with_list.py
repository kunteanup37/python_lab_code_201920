#This is a python code to show how we can use list to represent as stack
#List stk will be our stack
stk=[]
# while loop syntax 
while True:
	print("\nMenu\n1. Push Element.\n2. Display Stack\n3. Pop Element.\n4. Exit")
	ch=input("\nEnter your choice: ")
	if ch == '1':
		number=input("\nEnter element to push:")
		stk.append(int(number))
	elif ch == '2':
		#understand use of for loop and range function
		print("\nContents in stack are:")
		for i in range(len(stk)-1,-1,-1):
			print(stk[i])
		continue
	elif ch == '3':
		#understand use of try .... except to catch and deal with the exceptions at runtime  
		try:
			print("\nPopping element from stack...\n")
			element=stk.pop()
			print(str(element)+" is popped from stack.")
		except IndexError:
			print("\nSorry you cant pop element from empty list. \nFirst push somthing into stack.")
			continue
	elif ch =='4':
		#beaks the loop execution. 
		break
	else:
		pass

'''
This is multiline comment
Sample Run of above code 

 python stk_with_list.py 

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 1

Enter element to push:21

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 2

Contents in stack are:
21

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 1

Enter element to push:234

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 1

Enter element to push:4

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 2

Contents in stack are:
4
234
21

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 3

Popping element from stack...

4 is popped from stack.

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 2

Contents in stack are:
234
21

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 3

Popping element from stack...

234 is popped from stack.

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 3

Popping element from stack...

21 is popped from stack.

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 3

Popping element from stack...


Sorry you cant pop element from empty list. 
First push somthing into stack.

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 1

Enter element to push:32

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 3

Popping element from stack...

32 is popped from stack.

Menu
1. Push Element.
2. Display Stack
3. Pop Element.
4. Exit

Enter your choice: 4

'''


