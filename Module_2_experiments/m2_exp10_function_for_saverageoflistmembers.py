#Different ways to find average of a list

#First Method pure python 
# Python method to get average of a list 
def Average(lst): 
    return sum(lst) / len(lst) 
  
lst = [15, 9, 55, 41, 35, 20, 62, 49] 
average = Average(lst) 
#Printing average of the list 
print("Average of the list =", round(average, 2)) 

#Second Method using statistics API mean

# importing mean() 
from statistics import mean 
  
def Average_sm(lst): 
    return mean(lst) 
  
# Driver Code 
lst = [15, 9, 55, 41, 35, 20, 62, 49] 
average = Average_sm(lst) 
  
# Printing average of the list 
print("Average of the list =", round(average, 2)) 


# Method 3: Python program to get average of a list 
# Using reduce() and lambda  
  
# importing reduce() 
from functools import reduce
  
def Average_rl(lst): 
    return reduce(lambda a, b: a + b, lst) / len(lst) 
  
# Driver Code 
lst = [15, 9, 55, 41, 35, 20, 62, 49] 
average = Average_rl(lst) 
  
# Printing average of the list 
print("Average of the list =", round(average, 2)) 
 	
