import sys
#m and n must be positive integers
def ackermann(m, n):
    if m == 0:
        return n+1
    if n == 0:
        return ackermann(m-1, 1)
    return ackermann(m-1, ackermann(m, n-1))
#Acccept Commandline arguments

m=int(sys.argv[1])
n=int(sys.argv[2])
if m > 0 and n > 0:
    print(ackermann(m, n))
else:
    print("\n input must be positive integers.") 
