
#Note this function definition it takes a string argument and returns a dictionary composed of
#number of words and characters per line.
def count_words_chars(message):
	lines=message.split('\n')
	wc_c_dict={}
	lcnt=0
	for line in lines:
		words=line.split()
		wc_c_dict[lcnt]=(len(words),len(line))
		lcnt+=1
	return wc_c_dict

# This function accepts the dictionary and three flags l=count line, w=count words, c=count characters returns list summarizing line count, word count and char count
def wc(dict, l=0, w=0, c=0):
	res=[]
	lcnt=0
	ccnt=0
	if l == 1:
		res.insert(0,len(dict))
	else:
		res.insert(0,0)
	if w == 1 or c == 1:
		for _, value in dict.items():
			lcnt+=value[0]
			ccnt+=value[1]
	res.insert(1,lcnt)
	res.insert(2,ccnt)
	return res
#define to strings
str1="This is sample program.\nIt illustrates us how does string splitting works.\nIt also shows how we can use dictionaries.\n"
str2="This programs defines two multiline strings namelt str1 and str2.\nWe first join them using + operator."
#concatinate two strings
str=str1+str2
#original words
print("Original String:\n"+str+"\n")
#function call to count_words_chars
d=count_words_chars(str)
#count only number of lines
l,_,_=wc(d,1,0,0)
print("String has %d lines"%(l))
#count only number of words and characters
_, w, c=wc(d,0,1,1)
print("String has %d words  and %d characters"%(w,c))
#count only number of lines and words
l, w, _=wc(d,1,1,0)
print("String has %d words  and %d lines"%(w,l))
#count number of lines, words and characters
l, w, c=wc(d,1,1,1)
print("String has %d lines, %d words, %d characters"%(l,w,c))
#Only pass parameter to count lines
l,_,_=wc(d,1)
print("String has %d lines"%(l))

''' Output of the script
Original String:
This is sample program.
It illustrates us how does string splitting works.
It also shows how we can use dictionaries.
This programs defines two multiline strings namelt str1 and str2.
We first join them using + operator.

String has 5 lines
String has 37 words  and 216 characters
String has 37 words  and 5 lines
String has 5 lines, 37 words, 216 characters
'''
