#Example to show use of default arguments
def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries == 0:
            raise ValueError('invalid user response')
        print(reminder)
#call with only one parameter
print("ask_ok called with one parameter")
print(ask_ok("Enter y, ye or yes to continue...\n "))
#call with two parameters
print("ask_ok called with two parameters")
print(ask_ok("Enter y, ye or yes to continue...\n ",3))
#call with two parameters
print("ask_ok called with three parameters")
print(ask_ok("Enter y, ye or yes to continue...\n ",2,'You must try again'))

''' Output
 
ask_ok called with one parameter
Enter y, ye or yes to continue...
 yess
Please try again!
Enter y, ye or yes to continue...
 yes
True
ask_ok called with two parameters
Enter y, ye or yes to continue...
 n   
False
ask_ok called with three parameters
Enter y, ye or yes to continue...
 
You must try again
Enter y, ye or yes to continue...
 yess
Traceback (most recent call last):
  File "m2_exp6_default_arg_function.py", line 21, in <module>
    print(ask_ok("Enter y, ye or yes to continue...\n ",2,'You must try again'))
  File "m2_exp6_default_arg_function.py", line 11, in ask_ok
    raise ValueError('invalid user response')
ValueError: invalid user response

'''
