#Write a python program to implement automata M ={{S,A,B},{a,b},S,{B},some delta}
#Transition function for Automata is:
def S(str):
	try:
		sym=str[0]
	except IndexError:
		print("String Terminated in Sate S. Not Accepted")
		return
	if sym == 'a':
		A(str[1:])
	else:
		print("\nNO MOVE DEFINED HALTED")

def A(str):
	try:
		sym=str[0]
	except IndexError:
		print("String Terminated in Sate A. Not Accepted")
		return
	if sym == 'a':
		A(str[1:])
	elif sym == 'b':
		B(str[1:])
	else:
		print("\nNO MOVE DEFINED. HALTED")

def B(str):
	try:
		sym=str[0]
	except IndexError:
		print("String Terminated in Sate B. Accepted")
		return
	if sym == 'a':
		A(str[1:])
	elif sym == 'b':
		B(str[1:])
	else:
		print("\nNO MOVE DEFINED")

#input to automata x=abbba
x="abbba"
print("Input to M is:"+x)
S(x)
x="abab"
print("Input to M is:"+x)
S(x)
