f=open('input.txt', "r") 
dict={} 
for line  in f:
	for word in line.split(" "):
		word=word.strip('\n').strip('.') 
		if word not in dict.keys(): 
			dict[word]=1
		else: 
			dict[word]+=1 
f.close()       
rd={} 
for k, v in dict.items():
	if v not  in rd.keys():
		rd[v]=[k]
	else:
		rd[v].append(k) 
fqlist=list(rd.keys())
fqlist.sort()
fqlist.reverse()
fout=open('vocab.txt','w')
for freq in fqlist:
	for word in rd[freq]:
		s=word+':'+str(freq)+'\n'
		fout.write(s)
