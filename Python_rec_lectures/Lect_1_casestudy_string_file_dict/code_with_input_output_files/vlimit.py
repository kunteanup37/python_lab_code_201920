def  limit_vocab(input, vocab, vocab_limit):
	fin=open(input,'r')
	fvo=open(vocab,'r')
	fot=open("output_"+str(vocab_limit),'w')
	i=0
	vocab_list=[]
	for  line in fvo:
		word=line.split(':')[0]
		vocab_list.append(word)
		i+=1
		if i == vocab_limit:
			break
	fvo.close()
	start=0
	eos=0
	for line in fin:
		if start>0:
			fot.write('\n')
		for word in line.split():
			if word.endswith('.'):
				eos=1
			tword=word
			tword.strip('\n').strip('.')
			if tword not in vocab_list:
				tword="<UNK>"
			if eos == 1:
				tword=tword+'.'
			fot.write(" "+tword)
			eos=0
			eol=0
			if start ==0:
				start=1
	fot.write('\n')				
	fin.close()
	fot.close()

limit_vocab("input.txt", "vocab.txt", 10)
