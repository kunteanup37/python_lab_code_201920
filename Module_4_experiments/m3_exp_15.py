#Here we learn how to check if a file exists. How to delete it. 
import os
#check if file named temp.dat exists in present working directory
if os.path.exists("temp.dat"):
	os.remove("temp.dat")
	print("Deleted temp.dat")
else:
	print("The file does not exist") 
''' Output of sample run when no file named temp.dat present
$ python3 m3_exp_15.py 
The file does not exist
$ls
data_exp_13.txt  dict_output.pkl  dict_output.txt  m3_exp_13.py  m3_exp_14.py  m3_exp_15.py  m3_exp_16.py  neat_dict_output.dat
'''
''' Output of sample run when file named temp.dat exists
$ touch temp.dat
$ ls
data_exp_13.txt  dict_output.pkl  dict_output.txt  m3_exp_13.py  m3_exp_14.py  m3_exp_15.py  m3_exp_16.py  neat_dict_output.dat  temp.dat
$ python3 m3_exp_15.py 
Deleted temp.dat
$ls
data_exp_13.txt  dict_output.pkl  dict_output.txt  m3_exp_13.py  m3_exp_14.py  m3_exp_15.py  m3_exp_16.py  neat_dict_output.dat
'''

