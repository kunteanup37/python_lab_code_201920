#Function we designed earlier to find number of words and characters per line
#in given input message.
def count_words_chars(message):
	lines=message.split('\n')
	wc_c_dict={}
	lcnt=0
	for line in lines:
		if len(line) > 0:
			words=line.split()
			wc_c_dict[lcnt]=(len(words),len(line))
			lcnt+=1
	return wc_c_dict
#open  existing file named data_exp-13.txt in read, text mode. 
# This is text file that has 
#Create handle f
f=open("data_exp_13.txt","r")

#Know type of file handle
print("File Handle:"+str(type(f)))

# Iterate through the file contents and gather contents in string lines
lines=""
for line in f:
	lines=lines+line

#Close the file
f.close()

#Print the contents
print("File Contents:\n"+lines+"_____\n")

#Call count_words_chars method 
dict={}
dict=count_words_chars(lines)
#Print the dictionary returned by function
print(dict)


print("Saving dict into text file: dict_output.txt")
#Open another file dict_output.txt in write, text mode 
of=open("dict_output.txt",'w')
#write entire dictionary. Remember write function writes a string.
str_dict=str(dict)
of.write(str_dict)

#close output file
of.close()

print("Saving dict neatly into text file:neat_dict_output.dat")
#Open another file dict_output.txt in write, text mode 
of=open("neat_dict_output.dat",'wt') #note file extension here is just placeholder.
#write entire dictionary as TAB separated file.
for k,v in dict.items():
	line=str(k)+"\t"+str(v)+"\n"
	of.write(line)
#close output file
of.close()

#Storing a serialized object using Pickle class object
import pickle as pk  #first time we are using as keyword

#Pickle the dict
f=open("dict_output.pkl","wb")
pk.dump(dict,f)

#Unpicle the dict
dict_read=pk.load(open("dict_output.pkl","rb"))

print(dict_read)

''' Output Sample run
File Handle:<class '_io.TextIOWrapper'>
File Contents:
This is file.
You read it and pass data to function.
Function will tell us how many words, characters, lines are present.
We will then save the returned dict to a file.
_____

{0: (3, 13), 1: (8, 38), 2: (11, 68), 3: (10, 46)}
Saving dict into text file: dict_output.txt
Saving dict neatly into text file:neat_dict_output.dat
{0: (3, 13), 1: (8, 38), 2: (11, 68), 3: (10, 46)}

'''
