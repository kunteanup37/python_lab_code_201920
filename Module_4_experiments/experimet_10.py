'''
Experiment 10 code
Menu: 
1. Get File Name:	Input a file name to open a file.
2. File Exists:		Check if  file exists and report the same to user and ask to input another file name if file is already present.
3. Store Dictionary:	Create a dictionary month_number : days_in_month  store it in a above file.
4. Calculate Days:	Input a month number from user, load the Dictionary from above file and find number of days from input month number till end of year.
5. Exit
'''
import os
def display_menu():
	print("Choose Any of the following operation.")
	print("\n1. Get File Name.\n2. File Exists.\n3. Store Dictionary.\n4. Calculate Days Remaining.\n 5.Exit.\nEnter your option:")

def store_dict(file_name):
	d={1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31}
	h=open(file_name,"wt")
	h.write("Mth Days")
	for k,v in d.items():
		h.write(str(k)+" "+str(v))
	h.close()
	return d
#inputs month_days dictionary and returns bun of days remaining in cal year 
def cal_days_rem(d):
	rem_days=0
	mth=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug",\
 "Sep","Oct","Nov","Dec"]
	cur_mth=input("\nEnter month code (eg Mar for March):")
	idx=mth.index(cur_mth)
	for i in range(idx+1,13):
		rem_days+=d[i]
	return rem_days

if __name__ == '__main__':
	file_name=""
	file_ex=True
	dict_stored=False
	display_menu()
	while(True):
		op=input("\nEnter your choice:")
		if op == '5':
			print("Thanks for using our program")	
			break;
		elif op == '1':
			file_name=input("\nEnter the file name:")
		elif op == '2':
			print("\nCheking if file {} exists.".format(file_name))
			if file_name == "":
				print("\nFirst execute option 1 and then comeback.")
			elif os.path.exists(file_name):
				print("\n{} file exists.".format(file_name))
				print("\nChecking if dictionary created.")
				if dict_stored == False:
					print("\nNo Dictionary created. Go to first  menu and change the file name.")
				else: 
					print("\nCreate dictionary by excutin Option 3 Next.")
			else:
				print("\n{} file does not exist.".format(file_name))
				print("You can proceed for next menu.")
				file_ex=False
		elif op == '3':
			if file_ex == False:
				d=store_dict(file_name)
				print("Created and Stored Dictionary in {} file.".format(file_name))
				dict_stored=True
			else:
				print("\nCant create and store dictionary. First execute option 1, 2 and then come back.")
		elif op == '4':
			if dict_stored == True:
				days_rem=cal_days_rem(file_name,d)
				print(str(days_rem)+" days remain in year.")
			else:
				print("\nFirst execute option 1, 2, 3 and then come back.")
		
