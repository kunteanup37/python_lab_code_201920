class Polygon:
    def __init__(self, no_of_sides):
        self.n = no_of_sides
        self.sides = [0 for i in range(no_of_sides)]
    def inputSides(self):
        self.sides = [float(input("Enter side "+str(i+1)+" : ")) for i in range(self.n)]
    def dispSides(self):
        for i in range(self.n):
            print("Side",i+1,"is",self.sides[i])

class Triangle(Polygon):
    def __init__(self):
        Polygon.__init__(self,3)
    def findArea(self):
        a, b, c = self.sides
        # calculate the semi-perimeter
        s = (a + b + c) / 2
        area = (s*(s-a)*(s-b)*(s-c)) ** 0.5
        print('The area of the triangle is %0.2f' %area)

#Create triange object t which is derived from base class polynomial
t = Triangle()
# t accesses base class methods inputSides and display sides 
t.inputSides()
t.dispSides()

# t has its own method findArea 
t.findArea()

#Checks if t is instance of class Triangle
print(isinstance(t,Triangle))

#Checks if t is instance of class Polygon
print(isinstance(t,Polygon))

#Check if t is instance of class object
print(isinstance(t,object))

#checks if t is object of class int
print(isinstance(t,int))

#checks if  Polygon is sub class of Triangle or otherwise
print(issubclass(Polygon,Triangle))
print(issubclass(Triangle,Polygon))

#checks if bool is subclass of int class
print(issubclass(bool,int))

''' Sample run for above code 
Enter side 1 : 3
Enter side 2 : 4
Enter side 3 : 5
Side 1 is 3.0
Side 2 is 4.0
Side 3 is 5.0
The area of the triangle is 6.00
True
True
True
False
False
True
True

'''
