class MyClass:
	"Basics of declaration of class and object creation"
	a = 10
	def func1():
		print('Hello from func1')
	def func(self):
		print('Hello')
	def __str__(self):
		return "string overloading: __str__ Called when object is made str using str() Method"
print("Printing Class Meta data\n")
print("Access member variable a: %s"%MyClass.a)
print("Metadata about Class Method: %s"%MyClass.func)
print("Acessing Documentation about class: %s"%MyClass.__doc__)
b=MyClass()
print(str(b))
print(b.func())
b.func()
MyClass.func1()
print(MyClass.func())

''' Output of  a sample run :
Printing Class Meta data

Access member variable a: 10
Metadata about Class Method: <function MyClass.func at 0x7f02b414f950>
Acessing Documentation about class: Basics of declaration of class and object creation
string overloading: __str__ Called when object is made str using str() Method
Hello
None
Traceback (most recent call last):
  File "../m3_exp_09.py", line 15, in <module>
    print(MyClass.func())
TypeError: func() missing 1 required positional argument: 'self'




Printing Class Meta data

Access member variable a: 10
Metadata about Class Method: <function MyClass.func at 0x7f4aa77ba950>
Acessing Documentation about class: Basics of declaration of class and object creation
string overloading: __str__ Called when object is made str using str() Method
Hello
None
Traceback (most recent call last):
  File "example1.py", line 15, in <module>
    print(MyClass.func())
TypeError: func() missing 1 required positional argument: 'self'

'''
