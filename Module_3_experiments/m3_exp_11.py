class ComplexNumber:
    def __init__(self,r = 0,i = 0):
        self.real = r
        self.imag = i

    def getData(self):
        print("{0}+{1}j".format(self.real,self.imag))

#Creating objects c1 c2 and c3 by calling constructor
c1 = ComplexNumber(2,3)
c2 = ComplexNumber(r=2)
c3 = ComplexNumber(i=3)
# Call getData() function
c1.getData()
c2.getData()
c3.getData()
# Create a new attribute 'attr' for c2 object
c2.attr = 10

# print all member variable value  for c2 
print((c2.real, c2.imag, c2.attr))

#Access attr for c1 object
c1.attr


''' Sample run of abovecode 

2+3j
2+0j
0+3j
(2, 0, 10)
Traceback (most recent call last):
  File "../m3_exp_11.py", line 24, in <module>
    c1.attr
AttributeError: 'ComplexNumber' object has no attribute 'attr'

'''

