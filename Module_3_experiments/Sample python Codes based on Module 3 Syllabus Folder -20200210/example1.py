class MyClass:
	"Basics of declaration of class and object creation"
	a = 10
	def func(self):
		print('Hello')
	def __str__(self):
		return "stringoverloading: __str__ Clalled when object is made str using str() Method"
print("Printing Class Mata data\n")
print("Access member variable a: %s"%MyClass.a)
print("Matadata about Class Method: %s"%MyClass.func)
print("Acessing Documentation about class: %s"%MyClass.__doc__)
b=MyClass()
print(str(b))
print(b.func())
print(MyClass.func())

''' Output of  a sample run :

Printing Class Mata data

Access member variable a: 10
Matadata about Class Method: <function MyClass.func at 0x7f4aa77ba950>
Acessing Documentation about class: Basics of declaration of class and object creation
stringoverloading: __str__ Clalled when object is made str using str() Method
Hello
None
Traceback (most recent call last):
  File "example1.py", line 15, in <module>
    print(MyClass.func())
TypeError: func() missing 1 required positional argument: 'self'

'''
