class MyClass:
	"This is my second class"
	a = 10
	def func(self):
		print('Hello')
	def func2():
		print('I dont need "self" to be passed')

# create a new oject for MyClass
ob1 = MyClass()

#Metadata about MyClass.func 
print(MyClass.func)

#Metadata about ob1 copy of MyClass.func
print(ob1.func)

# Calling function func()
ob1.func()

#Calling function func2() by class
MyClass.func2()
MyClass.func()

''' Sample run of above code

<function MyClass.func at 0x7f19ddbea950>
<bound method MyClass.func of <__main__.MyClass object at 0x7f19df57c198>>
Hello
I dont need "self" to be passed
Traceback (most recent call last):
  File "example2.py", line 23, in <module>
    MyClass.func()
TypeError: func() missing 1 required positional argument: 'self'

'''
