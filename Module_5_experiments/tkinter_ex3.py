import tkinter as tk

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.count=2
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self) # Create and set properties of button
        self.hi_there["text"] = "Click Me Once"
        self.hi_there["command"] = self.say_hi # This is the click event handling function
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy) # Invoke the destructor
        self.quit.pack(side="bottom")

    def say_hi(self):
        if self.count >1:
            print("hi there, everyone! "+str(self.count))
            self.hi_there["text"] = "Click Again to quit"
            self.hi_there["command"]=self.master.destroy
        self.count-=1

root = tk.Tk()
app = Application(master=root)
app.mainloop()
