# Example is taken from DataCamp Course on python GUI
import tkinter
# We create object for main GuI of the current application called window with title
window = tkinter.Tk()
window.title("Button GUI")
# Create object for Button Widget and connect it to Parent GUI object and set required options
button_widget = tkinter.Button(window,text="Click Me")
button_widget.pack()

#Start the main loop of the program.
tkinter.mainloop()
